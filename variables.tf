variable "aws_secret" {
  description = "AWS Secret Key for terraform provider"
  type        = string
  sensitive   = true
}

variable "rds_pass" {
  description = "RDS DB admin pass"
  type        = string
  sensitive   = true
}

variable "vpc_block" {
  default = "10.42.0.0/16"
}


variable "vpc_sn1" {
  default = "10.42.1.0/16"
}

variable "vpc_sn2" {
  default = "10.42.2.0/16"
}

variable "vpc_sn3" {
  default = "10.42.3.0/16"
}