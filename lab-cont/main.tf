provider "aws" {
    access_key = "AKIASK6E73EZFAJO22WO"
    secret_key = var.secret_key
    region = "eu-west-2"
}

locals {
    ami = "ami-0fb391cce7a602d1f"
    type = "t2.micro"
    sshkey = "darren1"

}


resource "aws_instance" "demo1" {
    ami = local.ami
    instance_type = local.type
    key_name = local.sshkey
    vpc_security_group_ids = [aws_security_group.allow1.id]
    subnet_id = aws_subnet.subnet1.id
}

resource "aws_security_group" "allow1" {
  name        = "SSH"
  description = "SSH"
  vpc_id      = aws_vpc.vpc1.id

  ingress {
    description      = "Allow ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "terraform"
  }
}

resource "aws_s3_bucket" "b" {
  bucket = "terraform-dl123"

  tags = {
    Name        = "bucket123DL"
    Environment = "prd"
  }
}

resource "aws_s3_bucket_acl" "example1" {
  bucket = aws_s3_bucket.b.id
  acl    = "private"
}

output "vm_public_ip" {
    value = aws_instance.demo1.public_ip
}

output "s3_domain" {
   value = aws_s3_bucket.b.bucket_domain_name
}

resource "aws_vpc" "vpc1" {
  cidr_block       = var.vpc_block
  instance_tenancy = "default"

  tags = {
    Name = "terraform"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc1.id

  tags = {
    Name = "terraform" }
  }

resource "aws_subnet" "subnet1" {
  vpc_id = aws_vpc.vpc1.id
  availability_zone = "eu-west-2a"
  cidr_block = var.vpc_sn1
  map_public_ip_on_launch = true
  tags = {
    Name = "terraform"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id = aws_vpc.vpc1.id
  availability_zone = "eu-west-2a"
  cidr_block = var.vpc_sn2

  tags = {
    Name = "terraform"
  }
}

resource "aws_route_table" "rt1" {
  vpc_id = aws_vpc.vpc1.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "terraform"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.rt1.id
}
