# Mini Project LAB6
provider "aws" {
    access_key = "AKIASK6E73EZFAJO22WO"
    secret_key = var.aws_secret
    region = "eu-west-2"
    default_tags {
      tags = {
        Name = "MiniProjectLab6"
        Environment = "qa"
        Owner       = "Darren L"
        ManagedBy   = "Terraform"
      }
  }
}

locals {
    ami = "ami-0fb391cce7a602d1f"
    type = "t2.micro"
    sshkey = "darren1"

}

resource "aws_db_instance" "default" {
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "8.2"
  instance_class       = "db.t3.micro"
  db_name                 = "Darren"
  username             = "admin"
  password             = var.rds_pass
  parameter_group_name = "default.mysql8.2"
  skip_final_snapshot  = true
}

resource "aws_instance" "ci" {
    ami = local.ami
    instance_type = local.type
    key_name = local.sshkey
    vpc_security_group_ids = [aws_security_group.allow1.id]
    subnet_id = aws_subnet.subnet1.id
}

resource "aws_instance" "deploy" {
    ami = local.ami
    instance_type = local.type
    key_name = local.sshkey
    vpc_security_group_ids = [aws_security_group.allow1.id]
    subnet_id = aws_subnet.subnet2.id
}

resource "aws_security_group" "allow1" {
  name        = "SSH"
  description = "SSH"
  vpc_id      = aws_vpc.vpc1.id

  ingress {
    description      = "Allow ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}



output "ci_server_ip" {
    value = aws_instance.ci.public_ip
}

output "deploy_server_ip" {
    value = aws_instance.deploy.public_ip
}

output "rds_endpoint" {
    value = aws_db_instance.default.endpoint
}

resource "aws_vpc" "vpc1" {
  cidr_block       = var.vpc_block
  instance_tenancy = "default"
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc1.id

  }

resource "aws_subnet" "subnet1" {
  vpc_id = aws_vpc.vpc1.id
  availability_zone = "eu-west-2a"
  cidr_block = var.vpc_sn1
  map_public_ip_on_launch = true
}

resource "aws_subnet" "subnet2" {
  vpc_id = aws_vpc.vpc1.id
  availability_zone = "eu-west-2a"
  cidr_block = var.vpc_sn2
  map_public_ip_on_launch = true
}

resource "aws_route_table" "rt1" {
  vpc_id = aws_vpc.vpc1.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.rt1.id
}
