resource "aws_instance" "demo1" {
    ami = local.ami
    instance_type = local.type
    key_name = local.sshkey
    vpc_security_group_ids = [aws_security_group.allow1.id]
    subnet_id = aws_subnet.subnet1.id
}