variable "secret_key" {
  default = "BEFORE SECRET SEPERATION"
  sensitive = true
}



variable "vpc_block" {
  default = "10.42.0.0/16"
}


variable "vpc_sn1" {
  default = "10.42.4.0/24"
}

variable "vpc_sn2" {
  default = "10.42.15.0/24"
}